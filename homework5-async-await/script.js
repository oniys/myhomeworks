function LocationUser(continent, country, region, city, district) {
    this.continent = continent;
    this.country = country;
    this.region = region;
    this.city = city;
    this.district = district;
    this.elements = {
        parent: document.createElement('div'),
        continent: document.createElement('span'),
        country: document.createElement('span'),
        region: document.createElement('span'),
        city: document.createElement('span'),
        district: document.createElement('span'),
    }
}
LocationUser.prototype.render = function (container) {
    const {parent, continent, country, region, city, district} = this.elements
    continent.textContent = this.continent + ' '
    country.textContent = this.country + ' '
    region.textContent = this.region + ' '
    city.textContent = this.city + ' '
    district.textContent = this.district
    parent.append(continent, country, region, city, district)
    container.append(parent)
}
let btn = document.getElementById('ip-btn')
btn.addEventListener('click', async getIp => {
    let response = await fetch(`https://api.ipify.org/?format=json`, {
        method: "GET",
        headers: {
            'Content-type': 'application/json'
        }
    })
    let userIp = await response.json();
    getLocation(getIp, userIp)

})
async function getLocation(getIp, userIp) {
    let ress = await fetch(`http://ip-api.com/json/${userIp.ip}?fields=continent,country,region,city,district`, {
        method: "GET",
        headers: {
            'Content-type': 'application/json'
        }
    })
    let data = await ress.json()
    const user = new LocationUser(data.continent, data.country, data.region, data.city, data.district)
    user.render(document.getElementById('card'))


}



// function LocationUser(continent, country, region, city, district) {
//     this.continent = continent;
//     this.country = country;
//     this.region = region;
//     this.city = city;
//     this.district = district;
//     this.elements = {
//         parent: document.createElement('div'),
//         continent: document.createElement('span'),
//         country: document.createElement('span'),
//         region: document.createElement('span'),
//         city: document.createElement('span'),
//         district: document.createElement('span'),
//     }
// }
//
// LocationUser.prototype.render = function (container) {
//     const {parent, continent, country, region, city, district} = this.elements
//     continent.textContent = this.continent + ' '
//     country.textContent = this.country + ' '
//     region.textContent = this.region + ' '
//     city.textContent = this.city + ' '
//     district.textContent = this.district
//     parent.append(continent, country, region, city, district)
//     container.append(parent)
// }
//
// const button = document.createElement('button')
// button.textContent = 'Вычислить по IP';
// button.addEventListener('click', async getIp => {
//     let response = await fetch("http://api.ipify.org/?format=json", {
//         method: "GET",
//         headers: {'Content-type': 'application/json'}
//     })
//     let ipUser = await response.json();
//     await getLocation(ipUser)
// })
//
// async function getLocation(ipUser) {
//     let response = await fetch(`http://ip-api.com/json/${ipUser.ip}?fields=continent,country,region,city,district`, {
//         method: "GET",
//         headers: {'Content-type': 'application/json'}
//     })
//     let data = await response.json()
//     const user = new locationUser(data.continent, data.country, data.region, data.city, data.district)
//     user.render(document.getElementById('card'))
//
//
// }
// document.body.append(button);




// const button = document.createElement('button')
// const xhr = new XMLHttpRequest();
// button.addEventListener('click', () => {
//     xhr.open('GET', 'https://api.ipify.org/?format=json', true);
//     xhr.send();
//     xhr.onreadystatechange = function (response) {
//         if (xhr.readyState === 4 && xhr.status === 200) {
//             xhr.open('GET', 'http://demo.ip-api.com/json/' + JSON.parse(xhr.responseText).ip + '?fields=66842623&lang=en', true);
//             xhr.send()
//             xhr.onreadystatechange = function (response) {
//                 if (xhr.readyState === 4 && xhr.status === 200) {
//                     iWillSearchToIpRender(JSON.parse(xhr.responseText))
//                 }
//             }
//
//         }
//     }
// })
//
// function iWillSearchToIpRender({continent, country, region, regionName, zip}) {
//     document.body.insertAdjacentHTML('afterend',
//         `<ul>
//                 <li>${continent}</li>
//                 <li>${country}</li>
//                 <li>${region}</li>
//                 <li>${regionName}</li>
//                 <li>${zip}</li>
//                </ul>`);
// }

//
// button.textContent = 'Вычислить по IP'
// document.body.append(button)