// const list = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]
const hardList = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
let interval = null;
let counter = 3;
let timerCounterElement = document.createElement('div');
timerCounterElement.classList.add('timer');

renderList(hardList);
function renderList(arr, element = document.body) {
    const ul = createList(arr);
    element.insertAdjacentHTML('afterbegin',ul);
    interval = setInterval(cleanPage, 1000);
}

function createList(arr) {
   return `<ul>${createListItems(arr)}</ul>`;
}

function  createListItems(data) {
        return data.map(el => {
            if(Array.isArray(el)){
                return `<li><ul>${createListItems(el)}</ul></li>`
            }
            return  createItem(el);
        }).join('');
    }

function  createItem(el) {
return `<li>${el}</li>`;
}

function cleanPage() {
    if(counter === 0){
        clearInterval(interval);
        document.write();
    }else{
        timerCounterElement.textContent = counter;
        counter--;
        document.body.append(timerCounterElement);
    }

}