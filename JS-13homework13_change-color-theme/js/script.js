const swithTheamsBtn = document.querySelector('#theme')
const container = document.querySelector('.container-style')
let carrentTheme = localStorage.getItem('theme') || 'body-style-white';


window.addEventListener('load', init);

function init() {
    container.classList.add(carrentTheme);
}


swithTheamsBtn.addEventListener('click', onClickSwitchThemeHandler);


function  onClickSwitchThemeHandler() {
    swithTheame();
}


function swithTheame() {
    container.classList.remove(carrentTheme);
if(carrentTheme === 'body-style-white'){
    carrentTheme = 'body-style-dark';
}else {
    carrentTheme = 'body-style-white';
}
    container.classList.add(carrentTheme);
}

window.addEventListener('unload', ()=> {
    localStorage.setItem('theme',carrentTheme)
})