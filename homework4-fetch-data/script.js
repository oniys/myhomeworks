fetch('https://swapi.dev/api/films/')
    .then(res => res.json())
    .then(render)

const conteinerCart = document.createElement('ul')

function render({results}) {
    results.forEach(list => {
        list.characters.forEach(char => {
            fetch(char)
                .then(res => res.json())
                .then(function (person) {
                    person.episode_id = list.episode_id
                    personFilm(person)
                })
        })
        conteinerCart.insertAdjacentHTML("beforeend",
            `<li class="episode">Episode - ${list.episode_id}<ul id="${list.episode_id}"> <ul><li>${list.title}</li><li>${list.opening_crawl}</li></ul></ul></li>`)
    })
    document.body.append(conteinerCart)
}

function personFilm(person) {
    const idPerson = document.getElementById(person.episode_id);


    for (let i = 0; i < idPerson.parentElement.children.length; i++) {
        if (person.episode_id.toString() === idPerson.parentElement.children[i].id) {
            idPerson.parentElement.children[i].insertAdjacentHTML("afterbegin", `<li class="person-list">${person.name}</li>`)
        }
    }
    viewRender(idPerson)
}

function viewRender(idPerson) {
    idPerson.style.display = 'flex'
    idPerson.style.flexWrap = 'wrap'
    idPerson.style.listStyle = 'none'
}
