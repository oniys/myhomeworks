const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];

const containerRoot = document.getElementById('root')

function addElem () {
    containerRoot.append(document.createElement('ul'))

    books.forEach(function(value) {
    try {

      checkError(value);

      const li = document.createElement('li');
      li.textContent = `${value.name} ${value.author} ${value.price}`;

      document.querySelector("ul").append(li);
    }
    catch (err) {
      console.log(err)
    }
  });
}

function checkError(obj) {

  const propsName = ['name', 'author', 'price']
  for (let prop of propsName) {
    if (!obj.hasOwnProperty(prop)) {
      throw `Property doesn't exist: ${prop}`
    }
  }
}


addElem()