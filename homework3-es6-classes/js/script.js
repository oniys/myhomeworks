class Employee {

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {return this._name;}

    set name(value) {this._name = value;}

    get age() {return this._age;}

    set age(value) {this._age = value;}

    get salary() {return this._salary;}

    set salary(value) {this._salary = value;}
}

class Programmer extends Employee {
    constructor(name, age, salary) {
        super(name, age, salary);
        this.lang = "Rus,Urk,En"
    }

    get salary() {return this._salary * 3;}

    set salary(value) {this._salary = value;}
}

const user1 = new Programmer('Elena', 24, 2500)
const user2 = new Programmer('Yri', 34, 2100)
const user3 = new Programmer('Nikolai', 22, 4500)


console.log(user1)
console.log(user1.salary);

console.log(user2)
console.log(user2.salary);

console.log(user3)
console.log(user3.salary);
