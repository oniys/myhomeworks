let arrTest = ['hello', 'world', 23, '23', null]

//One metod
function filterBy(arr, dateType) {

    return arr.filter(el => typeof  el !== dateType)

}


// Second metod
// function filterBy(arr, dateType) {
//
//     const  res = [];
//
//     for (let el of arr) {
//         if(typeof  el !== dateType){
//             res.push(el)
//         }
//     }
// return res;
// }


//Two metod
// function filterBy(arr, dateType) {
//
//     const  res = [];
//
//     for (let i = 0; i < arr.length; i++) {
//         if(typeof  arr[i] !== dateType){
//             res.push(arr[i])
//         }
//     }
// return res;
// }
//



console.log(filterBy(arrTest,'string'))
console.log(filterBy(arrTest,'number'))