const slides = document.querySelectorAll('.images-wrapper img')
let activeSlide = slides[0];
let slidesCounter = 0;

const  {btnContinueSlider, btnStopSlider} = init();

let interval = setInterval(showNextSlide, 3000)
btnStopSlider.addEventListener('click',onClickStopHandler)

function onClickStopHandler() {
    clearInterval(interval)
    btnStopSlider.removeEventListener('click',onClickStopHandler)
    btnContinueSlider.addEventListener('click', onClickContinueHandler)
}

function onClickContinueHandler() {
    interval = setInterval(showNextSlide, 3000)
    btnContinueSlider.removeEventListener('click', onClickContinueHandler)
    btnStopSlider.addEventListener('click',onClickStopHandler)
}

function init() {
    const  btnStopSlider = document.createElement('button')
    btnStopSlider.textContent = 'Stop';
    document.body.append(btnStopSlider);

    const  btnContinueSlider = document.createElement('button')
    btnContinueSlider.textContent = 'Continue';
    document.body.append(btnContinueSlider);

    return {btnContinueSlider, btnStopSlider};
}

function showNextSlide() {
    slidesCounter++
    if(slidesCounter > slides.length-1) {
        slidesCounter = 0
    }
        activeSlide.classList.remove('active-slide');
        slides[slidesCounter].classList.add('active-slide');
        activeSlide = slides[slidesCounter];
}

