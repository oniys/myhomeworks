
const inputElement = initInputView()

inputElement.addEventListener('focus', inputFocusHandler)
inputElement.addEventListener('keypress', inputValidateHandler)
inputElement.addEventListener('blur', inputBlurHandler)
document.body.addEventListener('click', onClickContainerHandler)


function initInputView() {
    const container = document.createElement('div')
    const input = document.createElement('input')

    inputStyle(input);
    containerStyle(container);

    document.body.append(container);
    container.appendChild(input);
    return input
}

function inputStyle(input) {
    input.style.outline = 'none';
    input.style.border = '1px solid #000f';
    input.style.borderRadius = '3px';
    return;
}
function inputStyleFocus(input) {
    input.style.border = '1px solid #00ff00';
    input.style.color = '#00ff00';
    return;
}
function containerStyle(container) {
    container.style.display = 'flex';
    container.style.flexDirection = 'column';
    container.style.width = '15em';
    return;
}

function inputFocusHandler() {
    const inCorrectSpan = document.getElementById('retry');
    if(inCorrectSpan === null){
        inputStyleFocus(this);
    }else {
        inCorrectSpan.remove();
        inputStyleFocus(this);
    }
}

function inputValidateHandler(e) {
    if (!/[\d\-]/.test(e.key)) {
        e.preventDefault();
    }
}

function inputBlurHandler() {
    const inputValue = inputElement.value;

    if (inputValue === '') {
       inputStyle(this);
        return;
    }
    inputStyle(this);
    renderSpan(inputValue);
    inputElement.value = '';
}

function renderSpan(value) {
    const span = createSpan(value);
    inputElement.insertAdjacentHTML('beforebegin', span);
}

function createSpan(value) {
    if(value < 0){
       return`<span id="retry" style="order: 1; color: red" >Please enter correct price</span>`;
    }
    return`<span>Текущая цена: ${value} <button data-btn="delete">x</button></span>`;
}

function onClickContainerHandler({target}) {
    if (!target.dataset.btn) {
        return;
    }
    deleteSpan(target);
}

function deleteSpan(btn) {
    btn.closest('span').remove();
}

