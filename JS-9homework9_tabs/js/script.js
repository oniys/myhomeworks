const info = document.querySelectorAll('.tabs-content li')
const tab = document.querySelectorAll('.tabs-title')
const tabEvent = document.getElementById('eventClick');
tabEvent.addEventListener('click',function(event){
    switchTab(event)
});
function switchTab({target}) {
    const idTab = target.dataset.id
    for (let i = 0; i < tab.length; i++) {
        if (info[i].id === idTab) {
            tab[i].classList.add('active')
            info[i].classList.remove('content-text')
        } else {
            info[i].classList.add('content-text')
            tab[i].classList.remove('active')
        }
    }
}