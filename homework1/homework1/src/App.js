import React, {PureComponent} from 'react';
import Button from "./Components/Button/Button";
import Modal from "./Components/Button/Modal";

class App extends PureComponent {

    state = {
        isVisible: true,
        header: '',
        text: '',
        background: 'transparent',
        btnOk: '',
        btnCan: '',
        backgroundColorBtn: '',
        updateData: (value) => {
            this.setState({isVisible: value})
        }
    }

    render() {
        const styleBtn = {
            color: "black",
            fontSize: "10px",
            width: "100px",
            height: "30px",
            border: "1px solid #426e6f",
            margin: "10px",
            cursor: "pointer",
            outline: "none",
        }


        return (
            <div className="App">
                <header className="App-header">
                    <Button backgroundColorBtn={styleBtn} onClick={(s) => {

                        this.setState({
                            ...s,
                            header: 'Do you want to delete this file?',
                            text: "Onece you delete this file, it won't be possible to undo this action. Are you sure want to delete it? ",
                            background: "B53726",
                            btnOk: 'OK‍',
                            btnCan: 'Cancel',
                            backgroundColorBtn: "E94B35"

                        });

                    }} desc="Open first modal"/>
                    <Button backgroundColorBtn={styleBtn} onClick={(s) => {

                        this.setState({
                            ...s,
                            header: 'Do you want to copy this file?',
                            text: "When copying, you will receive a copy of the sample",
                            background: "3B3BE9",
                            btnOk: 'SAVE 🐈‍',
                            btnCan: 'Exit 🌙',
                            backgroundColorBtn: "d1d1d1"
                        });

                    }} desc="Open second modal"/>

                    <Modal visibles={this.state.isVisible}
                           header={this.state.header}
                           text={this.state.text}
                           background={this.state.background}
                           btnOk={this.state.btnOk}
                           btnCan={this.state.btnCan}
                           backgroundColorBtn={this.state.backgroundColorBtn}
                           updateData={this.state.updateData}
                    />
                </header>
            </div>
        );
    }
}

export default App;







