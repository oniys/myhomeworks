import React, {Component} from 'react';
import '../../CSS/Modal-style.scss'
import Button from "./Button";


class Modal extends Component {

    render() {
        const styleBtn = {
            color: "#fff",
            background: `#${this.props.background}`,
            borderRadius: "4px",
            margin: "10px",
            fontSize: "12px",
            width: "100px",
            height: "40px",
            border: "none",
            cursor: "pointer",
            outline: "none",
            flex: "0 0 auto",
        }
        const styleClo = {
            background: "transparent",
            color: "#fff",
            fontSize: "25px",
            border: "none",
            outline: "none",
            position: "relative",
            right: "27px",
            top: "15px"
        }
        const over = document.querySelector('.overlay');

        return (
            <div className="overlay" style={this.props.visibles ? {display: 'none'} : {display: 'block'}}
                 onClick={(s) => {
                     if (over === s.target) {
                         this.props.updateData({...s})
                     }
                 }}>
                <div className="window-modal" style={{background: `#${this.props.backgroundColorBtn}`}}>
                    <div className="boxTitle">
                        <h2 className="title">{this.props.header}</h2>
                        <Button backgroundColorBtn={styleClo} onClick={(s) => {
                            this.props.updateData({...s})
                        }} desc={`╳`}/>
                    </div>
                    <p className="text">{this.props.text}</p>
                    <div className="boxBtn">
                        <Button backgroundColorBtn={styleBtn}
                                onClick={() => alert(`Active - ${this.props.btnOk}`)}
                                desc={this.props.btnOk}/>
                        <Button backgroundColorBtn={styleBtn} onClick={(s) => {
                            this.props.updateData({...s})
                        }}
                                desc={this.props.btnCan}/>
                    </div>
                </div>
            </div>
        );
    }
}


export default Modal;