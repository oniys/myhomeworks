import React, {Component} from 'react';

export default class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {isToggleOn: true};
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState(state => ({
            isToggleOn: !state.isToggleOn
        }));
    }

    render() {
        return (
            <div>
                <button style={this.props.backgroundColorBtn} onClick={() => {
                    {
                        this.handleClick()
                        this.props.onClick(this.state.isToggleOn ? {isVisible: false} : {isVisible: false})
                    }
                }}>
                    {this.props.desc}
                </button>
            </div>
        );
    }
}
