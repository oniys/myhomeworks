const $page = $('html, body');
$('a[href*="#"]').click(function() {
    $page.animate({scrollTop: $($.attr(this, 'href')).offset().top}, 1000);
    return false;
});

$(document).ready(function() {
    let button = $('#button-up');
    $(window).scroll (function () {
        if ($(this).scrollTop () > 300) {
            button.fadeIn();
        } else {
            button.fadeOut();
        }
    });
    button.on('click', function(){
        $('body, html').animate({
            scrollTop: 0
        }, 750);
        return false;
    });
});

$( document ).ready(function(){
$( ".slide-toggle" ).click(function(){
    $( "#magazine" ).slideToggle();
});
});
