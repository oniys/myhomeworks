const collectionAmazingWork = document.querySelectorAll('.collection-group li')
const bntLoadMore = document.querySelector('.js-btn')
const tabsClick = document.querySelector('.js-tabs')
const loader = document.querySelector('.loader')
const swo = document.querySelector('.swo')


let activeTab = document.querySelector('.active')
let collectionCounter = 12;


renderCollectionAmazingWork(collectionCounter, activeTab.dataset.list);


bntLoadMore.addEventListener('click', onBtnHandler)
tabsClick.addEventListener('click', onTabsHandler)


function renderCollectionAmazingWork(collectionCounterToView, tab) {
    for (let i = 0; i < collectionCounterToView; i++) {

        console.log(collectionCounter);

        if (collectionAmazingWork.length < collectionCounter) {
            return
        }
        if (tab === 'all') {
            collectionAmazingWork[i].classList.remove('hidden');
        } else {
            if (collectionAmazingWork[i].dataset.items === tab) {
                collectionAmazingWork[i].classList.remove('hidden')
                // bntLoadMore.classList.add('hidden')
            } else {
                collectionAmazingWork[i].classList.add('hidden')
            }
        }
        if(collectionCounter === 36){
            collectionCounter = 12;
        }
    }
}

function onBtnHandler() {
    collectionCounter += 12;
    document.getElementById("button").disabled = true;
    bntLoadMore.style.background = '#a6e2d6';
    loader.classList.toggle("hidden");
    swo.style.opacity = 0.2;

    function loaderStart() {
        renderCollectionAmazingWork(collectionCounter, activeTab.dataset.list);
        loader.classList.toggle("hidden");
        document.getElementById("button").disabled = false;
        bntLoadMore.style.background = '#18CFAB';
        swo.style.opacity = 1;
    }
    setTimeout(loaderStart, 2200);

        if (collectionCounter === 36) {
        this.classList.add('hidden')
        // bntLoadMore.classList.add('hidden')
            collectionCounter = 12
        }
}


function onTabsHandler({target}) {

    if (activeTab === target) {
        return
    }

    activeTab.classList.remove('active')
    activeTab = target
    activeTab.classList.add('active')
    const tab = target.dataset.list;
    collectionCounter = collectionAmazingWork.length
    swo.style.opacity = 0.2;
    loader.classList.toggle("hidden");

    function tabLoaderStart() {

        if(tab === 'all') {
            renderCollectionAmazingWork(0, tab);
        }else {
            renderCollectionAmazingWork(collectionCounter, tab);
        }

        loader.classList.toggle("hidden");
        swo.style.opacity = 1;
    }

    setTimeout(tabLoaderStart, 1100);
}


