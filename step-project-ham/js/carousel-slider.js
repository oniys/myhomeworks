let position = 0;
let positionService = 0;

const slidersToShow = 1;
const slidersToScroll = 1;
const container = document.querySelector('.slider-container');
const track = document.querySelector('.slider-track');
const trackTabService = document.querySelector('.slider-track-service')
const btnPrev = document.querySelector('.btn-prev');
const btnNext = document.querySelector('.btn-next');

let activeTabServ = document.querySelectorAll('.services-list li')

document.body.addEventListener('click', onClickIcon)


const items = document.querySelectorAll('.slider-item');
const itemsCount = items.length;

const itemsTabSlider = document.querySelectorAll('.slider-item-service');

const itemsPagin = document.querySelectorAll('.js-pagin-item');
const itemsPaginCount = itemsPagin.length;


const itemWidth = container.clientWidth / slidersToShow;
let movePosition = slidersToScroll * itemWidth;

itemsPagin[position].style.transform = `translateY(-14px)`;

items.forEach((item) => {
    item.style.minWidth = `${itemWidth}px`;
});
itemsTabSlider.forEach((item) => {
    item.style.minWidth = `${itemWidth}px`;
});

btnNext.addEventListener('click', () => {
    const itemsLeft = itemsCount - (Math.abs(position) + slidersToShow * itemWidth) / itemWidth;
    if (itemsLeft === 0) {
        position = 0;
    } else {
        position -= itemsLeft >= slidersToScroll ? movePosition : itemsLeft * itemWidth;
    }
    setPosition(track);
});

btnPrev.addEventListener('click', () => {
    const itemsLeft = Math.abs(position) / itemWidth;
    if (itemsLeft === 0) {
        position = position - (movePosition * (itemsCount - 1));
    } else {
        position += itemsLeft >= slidersToScroll ? movePosition : itemsLeft * itemWidth;
    }
    setPosition(track);
});

const setPosition = (act) => {
    if (act.className === trackTabService.className) {
        act.style.transform = `translateX(${positionService}px)`
    } else {
        act.style.transform = `translateX(${position}px)`
        const numItem = Math.abs(position) / itemWidth;
        setPagin(numItem);
    }
};

function setPagin(numItem) {
    if (!isNaN(numItem)) {
        for (let i = 0; i < itemsPaginCount; i++) {
            if (numItem === i) {
                itemsPagin[i].style.transform = `translateY(-14px)`
            } else {
                itemsPagin[i].style.transform = `translateY(0px)`
            }
        }
    }

}

function onClickIcon({target}) {
    let positionPading = target.dataset.list;
    tabSlider(target);
    if (typeof positionPading !== "undefined") {
        position = itemWidth * -positionPading;
        setPosition(track);
        setPagin(+positionPading);
    }
}

function tabSlider(targets) {
    let positionTab = targets.dataset.listservis;
    if (typeof positionTab !== "undefined") {
        for (let i = 0; i < activeTabServ.length; i++) {
            activeTabServ[i].classList.remove("active-servis")
        }
        targets.classList.add('active-servis')
        positionService = -(itemWidth * positionTab - (itemWidth));
        setPosition(trackTabService);
    }
}

